import Swiper from 'https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.esm.browser.min.js'
const bar = document.getElementById('barChart').getContext('2d');
const doughnut = document.getElementById('doughnutChart').getContext('2d');
let user = {
  transactions: [
    {
      date: '11 Aug',
      amount: 20
    },
    {
      date: '12 Aug',
      amount: 40
    },
    {
      date: '15 Aug',
      amount: 25
    },
    {
      date: '17 Aug',
      amount: 20
    },
    {
      date: '18 Aug',
      amount: 50
    },
    {
      date: '20 Aug',
      amount: 30
    },
  ],
  percentageDevice: [63, 15, 22]
} 
function showInChart(user) {
  let deskPercentage = document.querySelector('.desktop__statistic')
  let tabletkPercentage = document.querySelector('.tablet__statistic')
  let mobPercentage = document.querySelector('.mobile__statistic')
  deskPercentage.innerHTML = user.percentageDevice[0] + '%'
  tabletkPercentage.innerHTML = user.percentageDevice[1] + '%'
  mobPercentage.innerHTML = user.percentageDevice[2] + '%'
  let devices = []
  let trans = []
  let dates = []
  for (let amount of user.transactions) {
    trans.push(amount.amount)
    dates.push(amount.date)
  }
  for (let d of user.percentageDevice) {
    devices.push(d)
  }
  let doughnutChart = new Chart(doughnut, {
    type: 'doughnut',
    data: {
      labels: [
        'Mobile',
        'Desktop',
        'Tablet'
      ],
      datasets: [{
        label: 'My First Dataset',
        data: devices,
        backgroundColor: [
          '#EC4C47',
          '#1070CA',
          '#F7D154'
        ],
        hoverOffset: 4
      }]
    }
  });
  let barChart = new Chart(bar, {
    type: 'bar',
    data: {
      labels: dates,
      datasets: [{
        label: '# of transactions',
        data: trans,
        backgroundColor: [
          '#1665D8',
          '#1665D8',
          '#1665D8',
          '#1665D8',
          '#1665D8',
          '#1665D8'
        ],
        borderColor: [
          '#1665D8',
          '#1665D8',
          '#1665D8',
          '#1665D8',
          '#1665D8',
          '#1665D8'
        ],
        borderWidth: 1,
        borderRadius: 10,
      }]
    },
    options: {
      scales: {
        y: {
          beginAtZero: true
        }
      }
    }
  });
  function color(arr, color) {
    for (let i = 1; i <= arr.length; i++) {
      `${color}`
    }
  }
}
showInChart(user)
